## DER

```mermaid
graph LR
A[Users] -- N --> B{Belong} -- N --> C[Cats]
```
---
| Users |Type  |
|-----|-----|
| id | int |
| name | varchar |
---
| Cats |Type  |
|-----|-----|
| id | int |
| name | varchar |
---
| Belong |Reference  |
|-----|-----|
| user | users (id) |
| cat | cats (id) |