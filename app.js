const express = require('express')
const mysql = require('mysql')
const myconn = require('express-myconnection')
const routes = require('./routes')
const cors = require('cors')

const app = express()
//agregando una propiedad a nuestra app
app.set('port', process.env.PORT || 9000)

const dboption = {
    host: 'localhost',
    port: 3306, //port connection database
    user: 'root',
    password: 'pw123',
    database: 'basedegatos'
}

// middlewares --------------
app.use(myconn(mysql, dboption, 'single'))
app.use(express.json())
app.use(cors())

// routes ---------------
app.get('/', (req, res)=>{
    res.send('Welcome to any api')
})
app.use('/api', routes)

// server running -------
app.listen(app.get('port'), ()=>{
    console.log('server running on port', app.get('port'))
})