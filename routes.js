const express = require('express')
const routes = express.Router()

//      RUTAS DE USUARIOS

routes.get('/users', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        conn.query('SELECT * FROM users', (err, rows)=>{
            if (err) return res.send(err)

            res.json(rows)
        })   
    })
})

routes.post('/users', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        console.log(req.body)
        conn.query('INSERT INTO users SET ?', [req.body], (err, rows)=>{
            if (err) return res.send(err)

            res.send('registrado!')
        })   
    })
})

routes.get('/users/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        conn.query('SELECT * FROM users WHERE id = ?', [req.params.id], (err,row) =>{
            if (err) return res.send(err)

            res.json(row)
        })
    })
})

routes.delete('/users/:id', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        //console.log(req.body)
        conn.query('DELETE FROM users WHERE id = ?', [req.params.id], (err, rows)=>{
            if (err) return res.send(err)

            res.send('eliminado!')
        })   
    })
})

routes.put('/users/:id', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        //console.log(req.body)
        conn.query('UPDATE users set ? WHERE id = ?', [res.body, req.params.id], (err, rows)=>{
            if (err) return res.send(err)

            res.send('actualizado!')
        })   
    })
})


//      RUTAS DE GATOS
routes.get('/cats', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        conn.query('SELECT * FROM cats', (err, rows)=>{
            if (err) return res.send(err)

            res.json(rows)
        })   
    })
})

routes.post('/cats', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        console.log(req.body)
        conn.query('INSERT INTO cats SET ?', [req.body], (err, rows)=>{
            if (err) return res.send(err)

            res.send('registrado!')
        })   
    })
})

routes.get('/cats/:id', (req, res)=>{
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        conn.query('SELECT * FROM cats WHERE id = ?', [req.params.id], (err,row) =>{
            if (err) return res.send(err)

            res.json(row)
        })
    })
})

routes.delete('/cats/:id', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        //console.log(req.body)
        conn.query('DELETE FROM cats WHERE id = ?', [req.params.id], (err, rows)=>{
            if (err) return res.send(err)

            res.send('eliminado!')
        })   
    })
})

routes.put('/cats/:id', (req, res)=> {
    req.getConnection((err, conn)=>{
        if (err) return res.send(err)

        //console.log(req.body)
        conn.query('UPDATE cats set ? WHERE id = ?', [res.body, req.params.id], (err, rows)=>{
            if (err) return res.send(err)

            res.send('actualizado!')
        })   
    })
})

module.exports = routes