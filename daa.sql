CREATE DATABASE basedegatos;
USE basedegatos;

CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20),
    PRIMARY KEY(id)
);

CREATE TABLE cats(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(20),
    PRIMARY KEY(id)
);

CREATE TABLE belong(
    user INT NOT NULL,
    cat INT NOT NULL,
    PRIMARY KEY (user, cat),
    FOREIGN KEY (user) REFERENCES users(id),
    FOREIGN KEY (cat) REFERENCES cats(id)
);

INSERT INTO users( name )
    VALUES
        ('Lessa'), ('Adrian'), ('Naty'), ('Ana');

INSERT INTO cats( name )
    VALUES
        ('Cocoa'), ('Shaka'), ('Garfield'), ('Cartucho');

INSERT INTO belong
        (user, cat)
    VALUES
        (1, 1), (2, 1);